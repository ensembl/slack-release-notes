const functions = require("firebase-functions");

const { logger } = functions;

exports.issuesMultiselectAction = async ({
  ack,
}) => {
  logger.info("issuesMultiselectAction fired");
  await ack();
};
