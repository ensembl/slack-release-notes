const axios = require("axios");

const functions = require("firebase-functions");

const { admin } = require("./admin");

const db = admin.firestore();
const { logger } = functions;
const config = functions.config();

const refreshAtlassianToken = async (teamId, userId) => {
  const snapshot = await db.collection(`teams/${teamId}/users/${userId}/secured`)
    .doc("atlassian").get();
  const { refresh_token: refreshToken } = await snapshot.data();

  try {
    const res = await axios({
      method: "POST",
      url: "https://auth.atlassian.com/oauth/token",
      data: {
        grant_type: "refresh_token",
        client_id: config.atlassian.client_id,
        client_secret: config.atlassian.secret,
        refresh_token: refreshToken,
      },
    });
    const { data } = res;
    await db.collection(`teams/${teamId}/users/${userId}/secured`).doc("atlassian").update(data);
  } catch (error) {
    logger.error("Error from Atlassian", error.response.status, error.response.statusText);
    await db.collection(`teams/${teamId}/users/`).doc(userId).update({ connectedToAtlassian: false });
  }
};

const callJiraTaskEndpoint = async ({
  id, accessToken, startAt,
}) => {
  const taskUrl = `https://api.atlassian.com/ex/jira/${id}/rest/api/3/search?startAt=${startAt}&maxResults=100&jql=updated>="-14d" AND issuetype="Task"&fields=summary,status,project,updated,issuetype,fixVersions`;

  const headers = { Authorization: `Bearer ${accessToken} ` };
  const res = await axios({
    method: "GET",
    url: taskUrl,
    headers,
  });

  const { data } = res;
  return data;
};

const callJiraEpicEndpoint = async ({ id, accessToken, startAt }) => {
  const epicUrl = `https://api.atlassian.com/ex/jira/${id}/rest/api/3/search?startAt=${startAt}&maxResults=100&jql=updated>="-90d" AND issuetype="Epic"&fields=summary,status,project,updated,issuetype,fixVersions`;

  const headers = { Authorization: `Bearer ${accessToken} ` };
  const res = await axios({
    method: "GET",
    url: epicUrl,
    headers,
  });

  const { data } = res;
  return data;
};

const getJiraCredential = async ({ teamId, userId }) => {
  const snapshot = await db.collection(`teams/${teamId}/users/${userId}/secured`).doc("atlassian").get();

  const { access_token: accessToken, id } = snapshot.data();
  return { accessToken, id };
};

const getJiraTasks = async ({ accessToken, id }) => {
  let page = 0;
  let startAt = 0;
  let shouldStop = false;
  const issues = [];
  while (!shouldStop && page < 20) {
    // eslint-disable-next-line
    const taskData = await callJiraTaskEndpoint({
      id, accessToken, startAt,
    });
    const { total } = taskData;

    const { issues: newIssues } = taskData;
    issues.push(...newIssues);
    startAt += 100;
    shouldStop = startAt >= total;
    page += 1;
  }
  return issues;
};

const getJiraEpics = async ({ accessToken, id }) => {
  let page = 0;
  let startAt = 0;
  let shouldStop = false;
  const issues = [];
  while (!shouldStop && page < 20) {
    // eslint-disable-next-line
    const taskData = await callJiraEpicEndpoint({
      id, accessToken, startAt,
    });
    const { total } = taskData;

    const { issues: newIssues } = taskData;
    issues.push(...newIssues);
    startAt += 100;
    shouldStop = startAt >= total;
    page += 1;
  }
  return issues;
};

const filterApiIssue = (issue) => {
  const { fields } = issue;
  return {
    id: issue.id,
    key: issue.key,
    project: {
      name: fields.project.name,
      id: fields.project.id,
      key: fields.project.key,
    },
    status: {
      name: fields.status.name,
      id: fields.status.id,
    },
    summary: fields.summary,
    updated: fields.updated,
    issueType: {
      name: fields.issuetype.name,
      subtask: fields.issuetype.subtask,
    },
    fixVersions: fields.fixVersions,
  };
};

const jiraSync = async (teamId, userId) => {
  const { accessToken, id } = await getJiraCredential({ teamId, userId });
  const issues = await getJiraTasks({ accessToken, id });
  const epics = await getJiraEpics({ accessToken, id });

  const cleanedIssues = [...issues, ...epics].map(filterApiIssue);

  const projects = {};

  cleanedIssues.forEach((issue) => {
    const { key } = issue.project;
    if (!(key in projects)) {
      projects[key] = issue.project;
    }
  });

  try {
    await db.collection(`teams/${teamId}/users/${userId}/projects`).add({
      createdAt: new Date(),
      projects: Object.values(projects),
    });
  } catch (error) {
    logger.error("Error storing projects", error);
  }

  try {
    await db.collection(`teams/${teamId}/users/${userId}/issues`).add({
      createdAt: new Date(),
      issues: cleanedIssues,
    });
  } catch (error) {
    logger.error("Error writing issues to firestore", error);
  }
};

const jiraScheduledJob = async () => {
  logger.info("15 mins Jira sync fired");
  const userDocs = await db.collectionGroup("users").get();
  // eslint-disable-next-line
  for (const userDoc of userDocs.docs) {
    const { connectedToAtlassian } = userDoc.data();
    if (connectedToAtlassian) {
      const teamId = userDoc.ref.parent.parent.id;
      const userId = userDoc.id;
      // eslint-disable-next-line
      await refreshAtlassianToken(teamId, userId);
      // eslint-disable-next-line
      await jiraSync(teamId, userId);
    }
  }
};

exports.jira15minsSync = functions.pubsub
  .schedule("*/15 * * * *")
  .onRun(jiraScheduledJob);

exports.jiraSync = jiraSync;

// Uncomment to run query to put in emulator database
// jiraScheduledJob();
