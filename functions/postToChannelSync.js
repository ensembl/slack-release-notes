const functions = require("firebase-functions");
const { getReleaseNoteSyncBlock } = require("./blocks/releaseNotesSync");
const { getDomainUrl } = require("./utils/jira");
const { addHistory } = require("./utils/history");
const { getUpdateSentBlock } = require("./blocks/updateSent");
const { getOriginalMessageBlock } = require("./blocks/originalMessage");

const { logger } = functions;

exports.postToChannelSync = async ({
  ack, body, client, view, context,
}) => {
  logger.info("postToChannelSync fired");

  const { state, private_metadata: viewId } = view;
  const { values } = state;
  const { id: userId } = body.user;
  const { userToken } = context;
  const teamId = body.view.team_id;
  const userText = values.update_text_input[`update_text_input-action-${viewId}`].value;
  const conversationIds = values.post_to_channel_select["channel_select-action"].selected_conversations;

  const mentionedUserIds = values.mention_users_multiselect["multi_users_select-action"].selected_users;

  await ack({
    response_action: "update",
    view: getUpdateSentBlock(teamId, conversationIds),
  });
  const url = await getDomainUrl(teamId, userId);

  const { blocks: updateBlocks, text: updateText } = await getReleaseNoteSyncBlock({
    url, userText, teamId, userId, mentionedUserIds,
  });

  conversationIds.map(async (id) => {
    const postMessageRes = await client.chat.postMessage({
      channel: id,
      blocks: updateBlocks,
      text: updateText,
      token: userToken,
    });
    const { ts: messageId, channel } = postMessageRes;
    const permaLinkRes = await client.chat.getPermalink({
      channel,
      message_ts: messageId,
    });

    const { blocks, text } = getOriginalMessageBlock(permaLinkRes.permalink, id);
    await client.chat.postMessage({
      channel: body.user.id,
      blocks,
      text,
      user: body.user.id,
    });
  });

  addHistory({
    teamId,
    userId,
    action: "post-to-channel",
    data: {
      userText, conversationIds, mentionedUserIds,
    },
  });
};
