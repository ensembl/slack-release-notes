const functions = require("firebase-functions");
const { admin } = require("./admin");

const { logger } = functions;
const db = admin.firestore();

exports.authorize = async (input) => {
  let userDoc;
  const { userId, teamId } = input;
  logger.info("Authorized fired", { teamId, userId });
  try {
    userDoc = await db.collection(`teams/${teamId}/users/${userId}/secured`).doc("slack").get();
  } catch (error) {
    functions.logger.error("Error getting user doc", { input });
  }

  const userToken = userDoc.exists ? userDoc.data().slackAuthedUser.access_token : null;
  let botId;
  let botToken;
  try {
    const teamDoc = await db.collection(`teams/${teamId}/secured`).doc("slack").get();
    ({ botUserId: botId, botAccessToken: botToken } = await teamDoc.data());
  } catch (error) {
    functions.logger.error("Error getting team doc", { input });
  }

  return {
    userToken,
    botId,
    botToken,
  };
};
