const admin = require("firebase-admin");

// Uncomment if running script on server
// const serviceAccount = require("./firebase-adminsdk-token.json");
// settings.credential = admin.credential.cert(serviceAccount);

const useEmulator = false;

const settings = {
  projectId: "slack-release-notes-890b8",
};

if (useEmulator) {
  process.env.FIRESTORE_EMULATOR_HOST = "localhost:8080";
}

admin.initializeApp(settings);

module.exports = { admin, useEmulator };
