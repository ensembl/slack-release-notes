const functions = require("firebase-functions");
const { admin } = require("./admin");

const db = admin.firestore();

const { logger } = functions;

exports.onPostToChannelAction = functions.firestore
  .document("teams/{teamId}/users/{userId}/history/{historyId}")
  .onCreate(async (snapshot, context) => {
    const { teamId, userId } = context.params;
    logger.info("onPostToChannelActionFired", userId);

    const { action, data } = snapshot.data();

    if (action === "post-to-channel") {
      const now = new Date();
      try {
        await db.collection(`teams/${teamId}/users/`).doc(userId).update({
          lastPostToChannelAt: now,
          lastActivity: now,
        });
      } catch (error) {
        logger.error("Error updating lastUpdateSentAt", error);
      }

      try {
        const { conversationIds, mentionedUserIds } = data;
        await db.collection(`teams/${teamId}/users`).doc(userId).update({
          lastConversationIds: conversationIds,
          lastMentionedUserIds: mentionedUserIds,
        });
      } catch (error) {
        logger.error("Error updating conversationIds", error);
      }
    }
  });
