const functions = require("firebase-functions");

const { logger } = functions;

const { getFirestoreIssues } = require("./utils/jira");
const { getReleaseConfigModalSyncBlock } = require("./blocks/releaseConfigSyncModal");

exports.updateHighlights = async ({ ack, body, client }) => {
  logger.info("updateHighlights fired");
  await ack();

  const viewId = body.view.private_metadata;
  const userText = body.view.state.values.update_text_input[`update_text_input-action-${viewId}`].value;
  const teamId = body.user.team_id;
  const userId = body.user.id;
  const selectedOptions = body.view.state.values.issues_multiselect["issues_multiselect_sync-action"].selected_options;
  const issues = await getFirestoreIssues(teamId, userId);
  const releaseConfigModalSyncBlock = await getReleaseConfigModalSyncBlock({
    issues: issues ? issues.slice(0, 100) : null,
    defaultToCurrentConversation: true,
    selectedOptions: selectedOptions || [],
    focusOnText: true,
    userText,
    teamId,
    userId,
  });

  await client.views.update({
    view_id: body.view.id,
    view: releaseConfigModalSyncBlock,
  });
};
