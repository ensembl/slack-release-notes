exports.getSlackOAuthPage = (teamId) => `<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ensembl</title>
</head>

<style>
body {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
}

.container {
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
}

</style>
<body>
<div class="container">
  <h1>Ensembl is now installed!</h1>
  <a href="slack://app?team=${teamId}&id=A034K5QM1JB&tab=messages">Return to Slack</a>
</div>
</body>

</html>`;
