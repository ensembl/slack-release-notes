const functions = require("firebase-functions");
const axios = require("axios");
const { App } = require("@slack/bolt");

const config = functions.config();
const { logger } = functions;
const { admin } = require("./admin");
const { updateHomeTab } = require("./updateHomeTab");
const { getSlackOAuthPage } = require("./pages/slackOAuthPage");
const { getOnboardMessage } = require("./blocks/onBoardMessage");

const { client_id: slackClientId, client_secret: slackClientSecret } = functions.config().slack;
const db = admin.firestore();

const slackOAuthEndpoint = "https://slack.com/api/oauth.v2.access";
const errorMessage = "Something went wrong. Close this window and try again in Slack.";

exports.slackOAuth = functions.https.onRequest(async (req, res) => {
  logger.info("slackOAuth fired");

  const { code, error: slackError } = req.query;

  if (slackError) {
    logger.info("User deny access");
    return res.status(200).redirect("https://ensembl.ai");
  }

  let oAuthRes;
  try {
    oAuthRes = await axios.post(slackOAuthEndpoint, `client_id=${slackClientId}&client_secret=${slackClientSecret}&code=${code}`);
  } catch (error) {
    functions.logger.error("Error posting to slackOAuthEndpoint", {
      slackOAuthEndpoint, slackClientId, slackClientSecret, code,
    });
    return res.status(400).send(errorMessage);
  }

  const {
    authed_user: slackAuthedUser, team,
    bot_user_id: botUserId,
    access_token: botAccessToken,
  } = oAuthRes.data;
  const { id: teamId, name: teamName } = team;
  const { id: slackUserId } = slackAuthedUser;
  const now = new Date();
  try {
    await db.collection("teams").doc(teamId).set({
      name: teamName,
      createdAt: now,
    }, { merge: true });
    await db.collection(`teams/${teamId}/secured`).doc("slack").set({
      createdAt: new Date(),
      botUserId,
      botAccessToken,
    });
  } catch (error) {
    functions.logger.error("Error setting team", { teamId });
  }

  // find user email
  const app = new App({
    signingSecret: config.slack.signing_secret,
    clientId: config.slack.client_id,
    processBeforeResponse: true,
    token: botAccessToken,
  });

  try {
    const userRes = await app.client.users.info({
      user: slackUserId,
    });
    const { user } = userRes;
    await db.collection(`teams/${teamId}/users/${slackUserId}/secured`).doc("slack").set({
      createdAt: now,
      slackAuthedUser,
    });
    await db.collection(`teams/${teamId}/users`).doc(slackUserId).set({
      createdAt: now,
      lastNotificationSentAt: now,
      lastPostToChannelAt: now,
      lastActivity: now,
      email: user.profile.email,
      displayName: user.profile.display_name,
      realName: user.profile.real_name,
    }, { merge: true });
  } catch (error) {
    functions.logger.error("Error getting user information", { slackUserId });
    return res.status(400).send(errorMessage);
  }

  await app.client.chat.postMessage({
    channel: slackUserId,
    blocks: getOnboardMessage(teamId, slackUserId),
  });

  // update home tab
  await updateHomeTab(teamId, slackUserId);

  return res.status(200).send(getSlackOAuthPage(teamId));
});
