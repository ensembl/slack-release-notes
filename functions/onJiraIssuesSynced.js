const functions = require("firebase-functions");
const { admin } = require("./admin");

const db = admin.firestore();

const { logger } = functions;

const { openAiCall, getConfig } = require("./utils/openai");

const { getRecommendationIssues } = require("./blocks/releaseConfigSyncModal");

exports.onJiraIssuesSynced = functions.firestore
  .document("teams/{teamId}/users/{userId}/issues/{issuesId}")
  .onCreate(async (snapshot, context) => {
    const { teamId, userId } = context.params;
    logger.info("onJiraIssuesSynced Fired", userId);

    const { issues } = snapshot.data();

    const recommendedIssues = await getRecommendationIssues({ issues, teamId, userId });

    recommendedIssues.map(async (issue) => {
      const suggestionSnapshot = await db.collection(`teams/${teamId}/users/${userId}/issueSuggestions`)
        .where("summary", "==", issue.summary)
        .get();
      if (suggestionSnapshot.empty) {
        const aiRes = await openAiCall(
          {
            data: getConfig({ issue }),
            endpoint: "text-davinci-002",
          },
        );
        const { text } = aiRes.data.choices[0];
        await db.collection(`teams/${teamId}/users/${userId}/issueSuggestions`).add({
          createdAt: new Date(),
          summary: issue.summary,
          aiSuggestion: text.trim().replaceAll("\n", ""),
        });
      }
    });
  });
