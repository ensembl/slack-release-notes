const functions = require("firebase-functions");

const { App, ExpressReceiver } = require("@slack/bolt");
const { postToChannelSync } = require("./postToChannelSync");
const { openChannel } = require("./openChannel");
const { updateHighlights } = require("./updateHighlights");

const { home } = require("./home");
const { connectAtlassian } = require("./connectAtlassian");
const { projectSelect } = require("./projectSelect");
const { authorize } = require("./authorize");

const { releaseConfig } = require("./releaseConfig");
const { releaseConfigSyncSlashCmd } = require("./releaseConfigSyncSlashCmd");
const { releaseConfigReminder } = require("./releaseConfigReminder");

const config = functions.config();

const expressReceiver = new ExpressReceiver({
  signingSecret: config.slack.signing_secret,
  endpoints: "/events",
  processBeforeResponse: true,
});

const app = new App({
  receiver: expressReceiver,
  signingSecret: config.slack.signing_secret,
  clientId: config.slack.client_id,
  processBeforeResponse: true,
  authorize,
});

app.message("hello", async ({ message, say }) => {
  await say(`Hello, <@${message.user}>`);
});

app.command("/jira-update", releaseConfigSyncSlashCmd);

app.action("update_highlights-action", updateHighlights);
app.action("atlassian_integration-action", connectAtlassian);
app.action("project_select-action", projectSelect);
app.action("create_update-action", releaseConfig);
app.action("create_update_reminder-action", releaseConfigReminder);
app.action("open_channel-action", openChannel);
app.view("post_to_channel_sync_id", postToChannelSync);
app.event("app_home_opened", home);

exports.slackApp = functions.https.onRequest(expressReceiver.app);
