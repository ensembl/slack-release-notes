const functions = require("firebase-functions");
const { App } = require("@slack/bolt");
const { admin } = require("./admin");

const db = admin.firestore();
const { logger } = functions;

const config = functions.config();

const { getReminderBlock } = require("./blocks/reminder");

const sendReminder = async () => {
  logger.info("sendReminder fired");
  const usersSnapshots = await db.collectionGroup("users").get();

  // eslint-disable-next-line
  for (const user of usersSnapshots.docs) {
    const {
      lastActivity, connectedToAtlassian,
    } = user.data();

    const now = new Date();
    const day = now.getDay();
    let dayLength;
    switch (day) {
      case 1:
      case 2:
      case 3:
        dayLength = 60 * 60 * 120 * 1000;
        break;
      case 4:
      case 5:
        dayLength = 60 * 60 * 72 * 1000;
        break;
      default:
    }
    const inactive = now - lastActivity.toDate() > dayLength;

    if (
      connectedToAtlassian && inactive && [1, 2, 3, 4, 5].includes(now.getDay())
    ) {
      logger.info(`Sending reminder for ${user.id}`);
      // eslint-disable-next-line
      const slackDoc = await user.ref.parent.parent.collection("secured").doc("slack").get();

      const { botAccessToken } = slackDoc.data();
      const app = new App({
        signingSecret: config.slack.signing_secret,
        clientId: config.slack.client_id,
        processBeforeResponse: true,
        token: botAccessToken,
      });

      try {
        // eslint-disable-next-line
        await app.client.chat.postMessage({
          channel: user.id,
          blocks: getReminderBlock(),
          text: "Reminder to create an update",
        });
      } catch (error) {
        logger.error(`Error sending reminder for ${user.id}`, error);
      }

      try {
        // eslint-disable-next-line
        await user.ref.update({
          lastNotificationSentAt: now,
          lastActivity: now,
        });
        // eslint-disable-next-line
        await user.ref.collection("history").add({
          action: "reminder-sent",
          createdAt: now,
        });
      } catch (error) {
        logger.error(`Error updating lastNotificationSentAt for ${user.id}`, error);
      }
    }
  }
};

exports.reminder = functions.pubsub
  .schedule("*/15 * * * 1-5")
  .onRun(sendReminder);

// Uncomment to run query to put in emulator database
// sendReminder();
