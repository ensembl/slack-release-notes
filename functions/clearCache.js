const functions = require("firebase-functions");

const { admin } = require("./admin");

const db = admin.firestore();
const { logger } = functions;

const deleteCache = async () => {
  logger.info("DeleteCache fired");
  const tsToMillis = admin.firestore.Timestamp.now().toMillis();
  const compareDate = new Date(tsToMillis - (6 * 60 * 60 * 1000));
  const oldCacheSnapshots = await db.collectionGroup("issues")
    .where("createdAt", "<", compareDate).get();
  oldCacheSnapshots.forEach(
    (doc) => { doc.ref.delete(); },
  );

  const oldProjectsCacheSnapshots = await db.collectionGroup("projects")
    .where("createdAt", "<", compareDate).get();
  oldProjectsCacheSnapshots.forEach(
    (doc) => { doc.ref.delete(); },
  );
};

exports.clearCache = functions.pubsub
  .schedule("30 * * * *")
  .onRun(deleteCache);

// Uncomment to run query to put in emulator database
// delete12HoursOldCache();
