const functions = require("firebase-functions");

const { logger } = functions;

const { getReleaseConfigModalSyncBlock } = require("./blocks/releaseConfigSyncModal");
const { getFirestoreIssues } = require("./utils/jira");
const { addHistory } = require("./utils/history");

exports.releaseConfigReminder = async ({
  ack, body, client,
}) => {
  logger.info("Release config reminder fired");
  await ack();

  const teamId = body.user.team_id;
  const userId = body.user.id;
  const issues = await getFirestoreIssues(teamId, userId);
  const releaseConfigModalBlock = getReleaseConfigModalSyncBlock({
    issues: issues ? issues.slice(0, 100) : null,
    defaultToCurrentConversation: true,
    selectedOptions: [],
    focusOnText: false,
  }); await client.views.open({
    trigger_id: body.trigger_id,
    view: releaseConfigModalBlock,
  });
  addHistory({ teamId, userId, action: "home/jira-update" });
};
