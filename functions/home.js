const functions = require("firebase-functions");
const { getHomeTab } = require("./blocks/homeTab");

const { logger } = functions;

exports.home = async ({ event, body, client }) => {
  logger.info("Home fired");
  await client.views.publish({
    user_id: event.user,
    view: await getHomeTab(body.authorizations[0].team_id, event.user),
  });
};
