const functions = require("firebase-functions");

const { logger } = functions;

const { getReleaseConfigModalSyncBlock } = require("./blocks/releaseConfigSyncModal");
const { getFirestoreIssues, getUserDefault } = require("./utils/jira");

const { addHistory } = require("./utils/history");

exports.releaseConfig = async ({
  ack, body, client,
}) => {
  logger.info("Release config fired");
  await ack();

  const teamId = body.view.team_id;
  const userId = body.user.id;
  const issues = await getFirestoreIssues(teamId, userId);
  const { defaultConversationIds, defaultUserIds } = await getUserDefault(teamId, userId);
  const releaseConfigModalBlock = await getReleaseConfigModalSyncBlock({
    issues: issues ? issues.slice(0, 100) : null,
    defaultToCurrentConversation: true,
    selectedOptions: [],
    focusOnText: false,
    defaultConversationIds,
    defaultUserIds,
    teamId,
    userId,
  });
  await client.views.open({
    trigger_id: body.trigger_id,
    view: releaseConfigModalBlock,
  });
  addHistory({ teamId, userId, action: "home/jira-update" });
};
