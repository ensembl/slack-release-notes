const functions = require("firebase-functions");

const { logger } = functions;

const { getFirestoreIssues, getUserDefault } = require("./utils/jira");
const { getReleaseConfigModalSyncBlock } = require("./blocks/releaseConfigSyncModal");

exports.releaseConfigSyncSlashCmd = async ({ ack, body, client }) => {
  logger.info("releaseConfigSyncSlashCmd fired");
  await ack();
  const teamId = body.team_id;
  const userId = body.user_id;

  const issues = await getFirestoreIssues(teamId, userId);
  const { defaultUserIds } = await getUserDefault(teamId, userId);

  const releaseConfigModalBlock = await getReleaseConfigModalSyncBlock({
    issues: issues ? issues.slice(0, 100) : null,
    defaultToCurrentConversation: true,
    selectedOptions: [],
    focusOnText: false,
    defaultUserIds,
    teamId,
    userId,
  });
  await client.views.open({
    trigger_id: body.trigger_id,
    view: releaseConfigModalBlock,
  });
};
