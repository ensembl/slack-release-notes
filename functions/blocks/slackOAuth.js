const functions = require("firebase-functions");

const config = functions.config();

exports.getSlackOAuthBlock = () => ([
  {
    type: "section",
    text: {
      type: "mrkdwn",
      text: "*Ensembl helps you create update with Jira ticket easily*",
    },
  },
  {
    type: "divider",
  },
  {
    type: "section",
    text: {
      type: "plain_text",
      text: "👇 Sign in with Slack to start using Ensembl",
      emoji: true,
    },
  },
  {
    type: "actions",
    elements: [
      {
        type: "button",
        text: {
          type: "plain_text",
          text: "Authorize with Slack",
          emoji: true,
        },
        url: `https://slack.com/oauth/v2/authorize?client_id=${config.slack.client_id}&scope=app_mentions:read,chat:write,chat:write.customize,chat:write.public,commands,im:history,users:read.email,users:read&user_scope=channels:write,chat:write,im:read,im:write,mpim:write`,
        style: "primary",
        value: "atlassian_integration",
        action_id: "atlassian_integration-action",
      },
    ],
  },
]);
