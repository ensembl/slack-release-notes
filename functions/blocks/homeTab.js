const functions = require("firebase-functions");
const { admin } = require("../admin");

const { getAtlassianOAuthBlock } = require("./atlassianOAuth");
const { getSlackOAuthBlock } = require("./slackOAuth");

const { logger } = functions;

const db = admin.firestore();

const getHeaderBlocks = (userId) => [
  {
    type: "section",
    text: {
      type: "mrkdwn",
      text: `* :wave: Welcome to Ensembl, <@${userId}>*`,
    },
  },
  {
    type: "divider",
  },
];

const getCreateUpdateBlock = () => ({
  type: "actions",
  elements: [
    {
      type: "button",
      text: {
        type: "plain_text",
        text: "Create Update",
        emoji: true,
      },
      style: "primary",
      value: "update",
      action_id: "create_update-action",
    },
  ],
});

exports.getHomeTab = async (teamId, userId) => {
  const userSnapshot = await db.collection(`teams/${teamId}/users`)
    .doc(userId)
    .get();

  if (!userSnapshot.exists) {
    logger.debug("User does not exist");
    return {
      // Home tabs must be enabled in your app configuration page under "App Home"
      type: "home",
      blocks: getSlackOAuthBlock(teamId, userId),
    };
  }

  const { connectedToAtlassian } = userSnapshot.data();
  if (!connectedToAtlassian) {
    logger.debug("User not connected to Atlassian");
    return {
      // Home tabs must be enabled in your app configuration page under "App Home"
      type: "home",
      blocks: [
        getAtlassianOAuthBlock(teamId, userId),
      ],
    };
  }

  const { selectedProjectKey } = await userSnapshot.data();
  const projectDoc = await userSnapshot.ref.collection("projects")
    .orderBy("createdAt", "desc")
    .limit(1)
    .get();
  const { projects } = await projectDoc.docs[0].data();

  if (selectedProjectKey) {
    return ({
      // Home tabs must be enabled in your app configuration page under "App Home"
      type: "home",
      blocks: [
        ...getHeaderBlocks(userId),
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: "Pick a project for updates",
          },
          accessory: {
            type: "static_select",
            placeholder: {
              type: "plain_text",
              text: "Select an item",
              emoji: true,
            },
            initial_option: projects
              .filter((project) => (project.key === selectedProjectKey))
              .map((project) => ({
                text: {
                  type: "plain_text",
                  text: project.name,
                  emoji: true,
                },
                value: project.key,
              }))[0],
            options:
              projects.map((project) => ({
                text: {
                  type: "plain_text",
                  text: project.name,
                  emoji: true,
                },
                value: project.key,
              })),
            action_id: "project_select-action",
          },
        },
        getCreateUpdateBlock(),
      ],
    });
  }
  return ({
    type: "home",
    blocks: [
      ...getHeaderBlocks(userId),
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Pick a project you want to see issues from",
        },
        accessory: {
          type: "static_select",
          placeholder: {
            type: "plain_text",
            text: "Select an item",
            emoji: true,
          },
          options:
            projects.map((project) => ({
              text: {
                type: "plain_text",
                text: project.name,
                emoji: true,
              },
              value: project.key,
            })),
          action_id: "project_select-action",
        },
      },
    ],
  });
};
