exports.getOriginalMessageBlock = (permalink, channelId) => {
  const blocks = [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `<${permalink}|Original message>`,
      },
    },
    {
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: `Update sent to <#${channelId}>`,
        },
      ],
    },
  ];
  const text = `<${permalink}|Original message>\nUpdate sent to <#${channelId}>`;
  return { blocks, text };
};
