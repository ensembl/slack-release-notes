const { v4: uuidv4 } = require("uuid");
const { admin } = require("../admin");

const db = admin.firestore();

const getMultiselectIssues = ({ issues, recommendedIssues }) => {
  const groups = {
    "Updated earlier today": [],
    "Updated more than 1 day ago": [],
    "Updated more than 3 days ago": [],
    "Updated more than 1 week ago": [],
  };

  const oneDay = 60 * 60 * 24 * 1000;
  const threeDays = 60 * 60 * 72 * 1000;
  const oneWeek = 60 * 60 * 168 * 1000;

  const now = new Date();
  issues.forEach((issue) => {
    const diff = now - new Date(issue.updated);
    if (diff < oneDay) {
      groups["Updated earlier today"] = [...groups["Updated earlier today"], issue];
    } else if (diff < threeDays) {
      groups["Updated more than 1 day ago"] = [...groups["Updated more than 1 day ago"], issue];
    } else if (diff < oneWeek) {
      groups["Updated more than 3 days ago"] = [...groups["Updated more than 3 days ago"], issue];
    } else {
      groups["Updated more than 1 week ago"] = [...groups["Updated more than 1 week ago"], issue];
    }
  });

  const template = {
    type: "input",
    optional: true,
    block_id: "issues_multiselect",
    element: {
      type: "multi_static_select",
      placeholder: {
        type: "plain_text",
        text: "Select issues",
        emoji: true,
      },
      option_groups: [],
      action_id: "issues_multiselect_sync-action",
    },
    label: {
      type: "plain_text",
      text: "📝 Select Jira issues",
      emoji: true,
    },
  };

  Object.entries(groups).forEach(([group, values]) => {
    if (values.length > 0) {
      template.element.option_groups = [
        ...template.element.option_groups,
        {
          label: {
            type: "plain_text",
            text: group,
          },
          options: values.map((issue) => {
            let stateEmoji;

            if (issue.issueType.name === "Epic") {
              stateEmoji = "🟪 ";
            } else {
              switch (issue.status.name) {
                case "Done":
                  stateEmoji = "✅ ";
                  break;
                case "To Do":
                  stateEmoji = "";
                  break;
                default:
                  stateEmoji = "🚧 ";
              }
            }

            return {
              text: {
                type: "plain_text",
                // need to make sure this is less than 151 characters per Slack
                text: `${issue.key} - ${stateEmoji}${issue.summary}`.slice(0, 75),
              },
              value: `${issue.key}`,
            };
          }),
        },
      ];
    }
  });

  if (recommendedIssues.length > 0) {
    const recommendedIssuesBlocks = recommendedIssues.map((issue) => ({
      text: {
        type: "plain_text",
        // need to make sure this is less than 151 characters per Slack
        text: `${issue.key} - ✅ ${issue.summary}`.slice(0, 75),
      },
      value: `${issue.key}`,
    }));
    template.element.initial_options = recommendedIssuesBlocks;
  }

  return template;
};

const getInputText = (selectedOptions, userText, recommendedIssues) => {
  // if there is no recommnded issue
  if (selectedOptions.length === 0 && recommendedIssues.length === 0) {
    return "Hi team!  This is now done. 🎉";
  }
  // Initial load put in recommended issue
  if (selectedOptions.length === 0 && !userText) {
    return userText || `Hi team!  This is now done. 🎉 \n\n${recommendedIssues.map((issue) => `• ${issue.key} - ✅ ${issue.summary}\n${issue.aiSuggestion ? `${issue.aiSuggestion}\n` : ""}`).join("\n")}
    `;
  }

  // If there is no selected options remove all bullet points
  if (selectedOptions.length === 0) {
    return userText.split("\n").filter((line) => !line.match("• ")).join("\n");
  }

  // Find the issues that are retained
  const projectKey = selectedOptions[0].value.split("-")[0];
  const regex = new RegExp(`^• ${projectKey}`);

  const selectedKeys = selectedOptions.map((option) => option.value);

  if (userText) {
    const previousKeys = [];
    // Find out what issues was previously included
    userText.split("\n").forEach((line) => {
      if (line.match(regex)) {
        previousKeys.push(line.split(" ")[1]);
      }
    });

    // Filtered out the text that matches the current selected options
    const filteredText = userText.split("\n").filter((line) => (
      !(line.match(regex)
        && !selectedKeys.includes(line.split(" ")[1]))
    )).join("\n");

    // add the new issue if the issue is not previously included
    const newIssues = selectedOptions
      .filter((option) => !previousKeys.includes(option.value))
      .map((issue) => `• ${issue.text.text}`);

    if (newIssues.length > 0) {
      return `${filteredText}\n${newIssues.join("\n")}`;
    }
    return `${filteredText}`;
  }

  // If the text box is empty, create starter text
  return `Hi team! This is now done. 🎉\n
${selectedOptions.map((option) => `• ${option.text.text}`).join("\n")}`;
};

const getTextBlock = ({
  selectedOptions, focusOnText, viewId, userText, recommendedIssues,
}) => ({
  type: "input",
  block_id: "update_text_input",
  element: {
    type: "plain_text_input",
    multiline: true,
    action_id: `update_text_input-action-${viewId}`,
    initial_value: getInputText(selectedOptions, userText, recommendedIssues),
    placeholder: {
      type: "plain_text",
      text: "Describe the update",
    },
    focus_on_load: focusOnText || false,
  },
  label: {
    type: "plain_text",
    text: "🚀 Message",
  },
});

const getNoIssuesMessage = () => ({
  type: "section",
  text: {
    type: "plain_text",
    text: "No issues found",
    emoji: true,
  },
});

const getRecommendationIssues = async ({ issues, teamId, userId }) => {
  const threeIssues = issues.filter((issue) => {
    const oneDay = 60 * 60 * 24 * 1000;
    const now = new Date();
    const diff = now - new Date(issue.updated);
    return issue.status.name === "Done" && diff < oneDay;
  }).slice(0, 3);

  return Promise.all(threeIssues.map(async (issue) => {
    const suggestionSnapshot = await db.collection(`teams/${teamId}/users/${userId}/issueSuggestions`)
      .where("summary", "==", issue.summary)
      .limit(1)
      .get();
    let aiSuggestion;
    if (!suggestionSnapshot.empty) {
      ({ aiSuggestion } = suggestionSnapshot.docs[0].data());
    }
    return { ...issue, aiSuggestion };
  }));
};

exports.getReleaseConfigModalSyncBlock = async ({
  issues,
  defaultToCurrentConversation,
  selectedOptions,
  focusOnText,
  userText,
  defaultConversationIds,
  defaultUserIds,
  teamId,
  userId,
}) => {
  const viewId = uuidv4();
  const recommendedIssues = await getRecommendationIssues({ issues, teamId, userId });
  return (
    {
      type: "modal",
      callback_id: "post_to_channel_sync_id",
      submit: {
        type: "plain_text",
        text: "Send as Me",
      },
      close: {
        type: "plain_text",
        text: "Cancel",
      },
      title: {
        type: "plain_text",
        text: "Create update from Jira",
        emoji: true,
      },
      private_metadata: viewId,
      blocks: [
        {
          type: "input",
          block_id: "post_to_channel_select",
          element: {
            type: "multi_conversations_select",
            placeholder: {
              type: "plain_text",
              text: "Select multiple",
            },
            action_id: "channel_select-action",
            initial_conversations: defaultConversationIds,
            default_to_current_conversation: defaultToCurrentConversation,
            filter: {
              exclude_bot_users: true,
            },
          },
          label: {
            type: "plain_text",
            text: "📨 Send to channel or DM",
            emoji: true,
          },
        },
        {
          type: "divider",
        },
        issues !== null && issues.length > 0
          ? getMultiselectIssues({ issues, recommendedIssues }) : getNoIssuesMessage(),
        {
          type: "actions",
          elements: [
            {
              type: "button",
              text: {
                type: "plain_text",
                text: "📋 Insert issues",
                emoji: true,
              },
              value: "update_highlights",
              action_id: "update_highlights-action",
              style: "primary",
            },
          ],
        },
        getTextBlock({
          issues, selectedOptions, focusOnText, viewId, userText, recommendedIssues,
        }),
        {
          type: "input",
          block_id: "mention_users_multiselect",
          optional: true,
          element: {
            type: "multi_users_select",
            placeholder: {
              type: "plain_text",
              text: "Select multiple",
              emoji: true,
            },
            initial_users: defaultUserIds,
            action_id: "multi_users_select-action",
          },
          label: {
            type: "plain_text",
            text: "😺 Tag someone",
            emoji: true,
          },
        },
      ],
    }
  );
};

exports.getRecommendationIssues = getRecommendationIssues;
