const { getAtlassianOAuthBlock } = require("./atlassianOAuth");

exports.getOnboardMessage = (teamId, userId) => (
  [
    {
      type: "header",
      text: {
        type: "plain_text",
        text: "🎉 Welcome to Ensembl 🎉",
        emoji: true,
      },
    },
    {
      type: "section",
      text: {
        type: "plain_text",
        text: "We are going to help you keep your team up-to-date by creating nicely formatted updates by automatically pulling in Jira tickets.",
        emoji: true,
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: "👇 Click to get started.",
      },
    },
    getAtlassianOAuthBlock(teamId, userId),
  ]
);
