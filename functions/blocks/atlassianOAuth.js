const { useEmulator } = require("../admin");

exports.getAtlassianOAuthBlock = (teamId, userId) => {
  const url = useEmulator
    ? `https://auth.atlassian.com/authorize?audience=api.atlassian.com&client_id=CTQfoVzUwuUPauyy0QGuf3Ir170Tte8a&scope=offline_access%20read%3Aproject%3Ajira%20read%3Ajql%3Ajira%20read%3Aissue%3Ajira-software%20read%3Asprint%3Ajira-software%20read%3Aboard-scope%3Ajira-software%20read%3Asource-code%3Ajira-software%20read%3Ajira-expressions%3Ajira%20read%3Aboard-scope.admin%3Ajira-software%20read%3Aavatar%3Ajira%20read%3Aissue%3Ajira%20read%3Aissue-meta%3Ajira%20read%3Aaudit-log%3Ajira%20read%3Afield-configuration%3Ajira%20read%3Aissue-details%3Ajira&redirect_uri=https%3A%2F%2F7b7d-216-171-3-218.ngrok.io%2Fslack-release-notes-890b8%2Fus-central1%2FatlassianOAuth-atlassianOAuth&state=${teamId},${userId}&response_type=code&prompt=consent`
    : `https://auth.atlassian.com/authorize?audience=api.atlassian.com&client_id=fbADJoq0nleOmgZj3wsYGFIzOtQTfV1j&scope=offline_access%20read%3Aavatar%3Ajira%20read%3Aaudit-log%3Ajira%20read%3Aissue%3Ajira%20read%3Aissue-meta%3Ajira%20read%3Afield-configuration%3Ajira%20read%3Aissue-details%3Ajira%20read%3Ajira-expressions%3Ajira%20read%3Aproject%3Ajira%20read%3Ajql%3Ajira%20read%3Aboard-scope.admin%3Ajira-software%20read%3Aboard-scope%3Ajira-software%20read%3Aissue%3Ajira-software%20read%3Asprint%3Ajira-software%20read%3Asource-code%3Ajira-software&redirect_uri=https%3A%2F%2Fus-central1-slack-release-notes-890b8.cloudfunctions.net%2FatlassianOAuth-atlassianOAuth&state=${teamId},${userId}&response_type=code&prompt=consent`;
  return ({
    type: "actions",
    elements: [
      {
        type: "button",
        text: {
          type: "plain_text",
          text: "Connect to Jira",
          emoji: true,
        },
        url,
        style: "primary",
        value: "atlassian_integration",
        action_id: "atlassian_integration-action",
      },
    ],
  });
};
