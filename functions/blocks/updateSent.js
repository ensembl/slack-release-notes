exports.getUpdateSentBlock = (teamId, conversationIds) => ({
  title: {
    type: "plain_text",
    text: "Create update from Jira",
  },
  blocks: conversationIds.map((id) => ({
    type: "section",
    text: {
      type: "mrkdwn",
      text: `🚀 Update sent to <#${id}> 🚀`,
    },
    accessory: {
      type: "button",
      text: {
        type: "plain_text",
        text: "Open Update",
      },
      url: `slack://channel?team=${teamId}&id=${id}`,
      value: "open_channel",
      action_id: "open_channel-action",
    },
  })),
  type: "modal",
});
