exports.getReminderBlock = () => (
  [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: "It’s been a while. Send an update to your team. 👉",
      },
      accessory: {
        type: "button",
        text: {
          type: "plain_text",
          text: "Create Update",
        },
        value: "update",
        action_id: "create_update_reminder-action",
      },
    },
  ]
);
