const constructJiraLink = (url, key) => `${url}/browse/${key}`;

const getLinkedUserText = ({ userText, url }) => userText
  .replaceAll(
    /[A-Z0-9]{2,}-\d+/g,
    (match) => `<${constructJiraLink(url, match)}|${match}>`,
  );

exports.getReleaseNoteSyncBlock = async ({
  url, userText, mentionedUserIds,
}) => {
  const linkedUserText = getLinkedUserText({
    userText, url,
  });
  const mentionedUsersText = mentionedUserIds.map((user) => `<@${user}>`).join(" ");
  const blocks = [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: linkedUserText,
      },
    },
  ];

  if (mentionedUserIds.length > 0) {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: mentionedUsersText,
      },
    });
  }

  return { blocks, text: `${linkedUserText}\n${mentionedUsersText}` };
};
