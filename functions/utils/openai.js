const axios = require("axios");
const functions = require("firebase-functions");

const { logger } = functions;
const OPENAI_KEY = functions.config().openai.key;

exports.openAiCall = async ({ data, endpoint }) => {
  logger.info("calling openAI", data);
  const response = await axios({
    url: `https://api.openai.com/v1/engines/${endpoint}/completions`,
    headers: {
      Authorization: `Bearer ${OPENAI_KEY}`,
      "Content-Type": "application/json",
    },
    data,
    method: "post",
  });
  return response;
};

const getPrompt = ({ issue }) => {
  const prompt = `Jira issue title: ${issue.summary}
  One sentence business benefit:`;
  return prompt;
};

exports.getConfig = ({ issue }) => ({
  prompt: getPrompt({ issue }),
  max_tokens: 100,
  temperature: 0.5,
  top_p: 1,
  user: "testing",
});

// const { getConfig, openAiCall } = require("./utils/openai");
// const aiRes = await openAiCall(
//   {
//     data: getConfig({ issues: filteredIssues }),
//     endpoint: "text-davinci-001",
//   },
// );

// const { data: aiData } = aiRes;
// console.log("AI DATA", aiData);
