const { admin } = require("../admin");

const db = admin.firestore();

exports.addHistory = ({
  teamId, userId, action, data = null,
}) => {
  db.collection(`teams/${teamId}/users/${userId}/history`).add({
    createdAt: new Date(),
    action,
    data,
  });
};
