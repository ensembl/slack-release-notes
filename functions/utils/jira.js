const { admin } = require("../admin");

const db = admin.firestore();

exports.getFirestoreIssues = async (teamId, userId) => {
  const userSnapshot = await db.collection(`teams/${teamId}/users`).doc(userId).get();
  const { selectedProjectKey } = userSnapshot.data();

  const issuesSnapshot = await db.collection(`teams/${teamId}/users/${userId}/issues`)
    .orderBy("createdAt", "desc")
    .limit(1)
    .get();

  if (issuesSnapshot.empty) {
    return null;
  }

  const data = await issuesSnapshot.docs[0].data();
  return data.issues
    .filter((issue) => (issue.project.key === selectedProjectKey))
    .sort((a, b) => new Date(b.updated) - new Date(a.updated));
};

exports.constructJiraLink = (url, issue) => {
  const issueKey = issue.key;
  return `${url}/browse/${issueKey}`;
};

exports.getDomainUrl = async (teamId, userId) => {
  const doc = await db.collection(`teams/${teamId}/users/${userId}/secured`).doc("atlassian").get();

  if (doc.exists) {
    const { url } = await doc.data();
    return url;
  }
  return "https://ensembl.atlassian.net";
};

exports.getUserDefault = async (teamId, userId) => {
  const userSnapshot = await db.collection(`teams/${teamId}/users`).doc(userId).get();
  const { lastConversationIds, lastMentionedUserIds } = await userSnapshot.data();
  return {
    defaultConversationIds: lastConversationIds,
    defaultUserIds: lastMentionedUserIds,
  };
};
