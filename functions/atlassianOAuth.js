const axios = require("axios");
const functions = require("firebase-functions");

const config = functions.config();

const { logger } = functions;
const { admin, useEmulator } = require("./admin");
const { jiraSync } = require("./jiraSync");
const { getAtlassianOAuthPage } = require("./pages/atlassianOAuthPage");
const { updateHomeTab } = require("./updateHomeTab");

const db = admin.firestore();

exports.atlassianOAuth = functions.https.onRequest(async (req, res) => {
  logger.info("atlassianOAuth oauth fired.");
  const { code, state } = req.query;
  const [teamId, userId] = state.split(",");
  const authUrl = "https://auth.atlassian.com/oauth/token";
  const redirectUri = useEmulator
    ? "https://7b7d-216-171-3-218.ngrok.io/slack-release-notes-890b8/us-central1/atlassianOAuth-atlassianOAuth"
    : "https://us-central1-slack-release-notes-890b8.cloudfunctions.net/atlassianOAuth-atlassianOAuth";

  const data = {
    grant_type: "authorization_code",
    client_id: config.atlassian.client_id,
    client_secret: config.atlassian.secret,
    code,
    redirect_uri: redirectUri,
  };
  const headers = {
    "Content-Type": "application/json",
  };
  const authRes = await axios({
    method: "POST",
    url: authUrl,
    headers,
    data,
  });
  const { data: authData } = authRes;

  await db.collection(`teams/${teamId}/users`).doc(userId).update({
    connectedToAtlassian: true,
  });

  const resourceRes = await axios({
    method: "GET",
    url: "https://api.atlassian.com/oauth/token/accessible-resources",
    headers: { Authorization: `Bearer ${authData.access_token}` },
  });
  const { data: resourceData } = resourceRes;
  await db.collection(`teams/${teamId}/users/${userId}/secured`).doc("atlassian").set(
    { ...authData, ...resourceData[0] },
  );

  await jiraSync(teamId, userId);

  // Update home tab
  await updateHomeTab(teamId, userId);

  res.status(200).send(getAtlassianOAuthPage(teamId));
});
