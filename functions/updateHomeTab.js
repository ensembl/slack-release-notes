const functions = require("firebase-functions");

const { App } = require("@slack/bolt");
const { admin } = require("./admin");

const db = admin.firestore();
const config = functions.config();
const { getHomeTab } = require("./blocks/homeTab");

exports.updateHomeTab = async (teamId, userId) => {
  const slackDoc = await db.collection(`teams/${teamId}/secured`).doc("slack").get();

  const { botAccessToken } = slackDoc.data();
  const app = new App({
    signingSecret: config.slack.signing_secret,
    clientId: config.slack.client_id,
    processBeforeResponse: true,
    token: botAccessToken,
  });

  await app.client.views.publish({
    user_id: userId,
    view: await getHomeTab(teamId, userId),
  });
};
