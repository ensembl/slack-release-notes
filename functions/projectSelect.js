const { admin } = require("./admin");
const { getHomeTab } = require("./blocks/homeTab");

const db = admin.firestore();

exports.projectSelect = async ({ client, ack, body }) => {
  const teamId = body.view.team_id;
  const userId = body.user.id;
  await ack();
  await db.collection(`teams/${teamId}/users`).doc(userId).update({
    selectedProjectKey: body.actions[0].selected_option.value,
  });

  client.views.publish({
    user_id: userId,
    view: await getHomeTab(teamId, userId),
  });
};
